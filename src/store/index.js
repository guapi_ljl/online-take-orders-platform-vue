import Vue from "vue";
import Vuex from "vuex";
import router from "@/route/index";

// 引入 api 方法
import {
	getRouteList
} from "@/api/common.js";

const ROUTERS = "ROUTES";
const MENUS = "MENUS";

const baseRoute = {
	path: "/layout",
	name: "Layout",
	component: () => import("../views/layout/index.vue"),
	children: []
};
const homeRoute = {
	path: "/home",
	name: "Home",
	component: () => import("../views/home/index.vue"),
	children: []
}
/**
 * 生成路由菜单列表
 * @param {Object} baseRoute baseRoute
 * @param {Object} routeList 路由列表
 */
function convertRoutes(baseRoute, routeList) {
	baseRoute.children = [];
	homeRoute.children = [];
	const result = []
	// console.log(routeList)
	routeList.forEach(item => {
		if (item.component != "none" || item.component) {
			// console.log("测试路由导入---path:%s component:%s", item.path, item.component);

			if (item.path.split('\/')[1] === 'home' || item.path.split('\/')[1] === 'common') {
				homeRoute.children.push({
					path: item.path,
					name: item.name,
					component: () => import("../views" + item.component)
				})
			}
			baseRoute.children.push({
				path: item.path,
				name: item.name,
				component: () => import("../views" + item.component)
			});
		}
	});
	result.homeRoute = homeRoute
	result.baseRoute = baseRoute
	console.log(result)
	return result
}

const actions = {
	async ACTION_GET_MENU(context, value) {
		// 获取路由列表..
		let menuStr = sessionStorage.getItem(MENUS);
		//如果获得了已有路由信息
		if (menuStr) {
			// 菜单 JSON
			var menuData = JSON.parse(menuStr);
			// 路由 JSON
			// console.log("menuData", menuData)
			let routeData = convertRoutes(baseRoute, menuData);
			// console.log(routeData)
			context.commit("MUTATION_GET_MENU", {
				baseRouteData: routeData.baseRoute,
				homeRouteData: routeData.homeRoute,
				menuData: menuData
			});
			return;
		}
		await getRouteList().then(resp => {
			// console.log("resp", resp);
			//转化后端为路由信息
			let routeData = convertRoutes(baseRoute, resp.rows);
			// console.log(routeData)
			const _data = {
				baseRouteData: routeData.baseRoute,
				homeRouteData: routeData.homeRoute,
				menuData: resp.rows
			};
			var routeStr = JSON.stringify(routeData);
			var menuStr = JSON.stringify(resp.rows);
			sessionStorage.setItem(MENUS, menuStr);
			context.commit("MUTATION_GET_MENU", _data);
		});
	},
	async ACTION_SAVE_USER(context, value) {
		context.commit("MUTATION_SAVE_USER", value);
	},
	async ACTION_TASK_DETAIL(context, value) {
		context.commit("MUTATION_TASK_DETAIL", value);

	}
};

const mutations = {
	MUTATION_GET_MENU(state, obj) {
		// console.log("[STORE]保存路由列表..");
		// 动态路由
		state.asynRouters = obj.baseRouteData;
		state.homeRoute = obj.homeRouteData
		state.menuData = obj.menuData;
		state.hasRoute = true;
		router.addRoute(state.asynRouters);
		router.addRoute(state.homeRoute)
		// console.log("state=====>", state);
	},
	MUTATION_SAVE_USER(state, obj) {
		state.user = obj;
	},
	MUTATION_TASK_DETAIL(state, detail) {
		state.data = detail
	}
};

const state = {
	menuData: [],
	hasRoute: false,
	asynRouters: {},
	homeRoute: {},
	user: "",
	data: []
};

// Vue.use(Vuex);
export default new Vuex.Store({
	actions: actions,
	mutations: mutations,
	state: state
});
