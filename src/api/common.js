import request from "@/utils/request";

export async function doLogin(data) {
	return await request({
		url: "/guapi/common/login",
		method: "post",
		data: data
	});
}
export async function toRegister(data, mailCode) {
	return await request({
		url: "/guapi/common/register/" + mailCode,
		method: "post",
		data: data
	});
}
export async function doLogout(data) {
	return await request({
		url: "/guapi/common/logout",
		method: "post",
		data: data
	});
}
export async function doForget(data) {
	return await request({
		url: "/guapi/common/forget",
		method: "post",
		data: data
	});
}
export async function getRandomCode(data) {
	return await request({
		url: "/guapi/common/random",
		method: "post",
		data: data
	});
}

export async function getRouteList() {
	return await request({
		url: "/guapi/common/routeList",
		method: "post",

	});
}

export async function sendMailCode(data) {
	return await request({
		url: "/guapi/common/mailCode",
		method: "post",
		data: data
	});
}
export async function doReset(data) {
	return await request({
		url: "/guapi/common/reset",
		method: "post",
		data: data
	});
}
export async function parseHeader(url) {
	return await request({
		url: "/guapi/common/header",
		method: "post",
		data: url,
		responseType: 'blob',
	});
}
