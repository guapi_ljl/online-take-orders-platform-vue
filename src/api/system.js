import request from "@/utils/request";

export function getSysInfo() {
	return request({
		url: "/guapi/system/info",
		method: "post",
	});
}
export function getIpList(param) {
	return request({
		url: "/guapi/system/ip/manage",
		method: "post",
		data:param
	});
}
