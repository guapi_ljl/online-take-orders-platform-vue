import request from "@/utils/request";

export function getUserList(param) {
	return request({
		url: "/guapi/user/list",
		method: "post",
		data: param
	});
}
export function addUser(param) {
	return request({
		url: "/guapi/user/add/" + false,
		method: "post",
		data: param
	});
}

export function deleteUser(param) {
	return request({
		url: "/guapi/user/delete",
		method: "post",
		data: param
	});
}

export function editUser(param) {
	return request({
		url: "/guapi/user/add/" + true,
		method: "post",
		data: param
	});
}

export function uploadHeader(param) {
	return request({
		url: "/guapi/user/header/upload",
		method: "post",
		data: param,
		headers: {
			"Content-Type": "multipart/form-data"
		}
	});
}
export function deleteHeader() {
	return request({
		url: "/guapi/user/header/delete",
		method: "post",
	});
}

export function getUserDetails(param) {
	return request({
		url: "/guapi/user/detail",
		method: "post",
		data: param
	});
}
export function getUsersHasMail() {
	return request({
		url: "/guapi/user/hasMail/"+true,
		method: "post"
	});
}
export function getUsersDontHasMail() {
	return request({
		url: "/guapi/user/hasMail/"+false,
		method: "post"
	});
}
export function getNameAndEmail(params) {
	return request({
		url: "/guapi/user/listByParams",
		method: "post",
		data:params
	});
}