import request from "@/utils/request";

export function getJobList(param) {
	return request({
		url: "/guapi/job/list",
		method: "post",
		data: param
	});
}
export function saveJob(param) {
	return request({
		url: "/guapi/job/save/" + false,
		method: "post",
		data: param
	});
}
export function updateJob(param) {
	return request({
		url: "/guapi/job/save/" + true,
		method: "post",
		data: param
	});
}
export function deleteJob(param) {
	return request({
		url: "/guapi/job/delete",
		method: "post",
		data: param
	});
}
