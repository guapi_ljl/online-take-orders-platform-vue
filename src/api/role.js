import request from "@/utils/request";
export function getRoleList(param) {
	return request({
		url: "/guapi/role/list",
		method: "post",
		data: param
	});
}
export function addRole(param) {
	return request({
		url: "/guapi/role/save",
		method: "post",
		data: param
	});
}
export function deleteRole(param) {
	return request({
		url: "/guapi/role/delete",
		method: "post",
		data: param
	});
}
export function changeAuth(param) {
	return request({
		url: "/guapi/role/auth/change",
		method: "post",
		data: param
	});
}
export function showAuth(param) {
	return request({
		url: "/guapi/role/auth/show",
		method: "post",
		data: param
	});
}
