import request from "@/utils/request";

export function uploadMail(param) {
    return request({
        url: "/guapi/mail/upload",
        method: "post",
        data: param,
        headers: {
            "Content-Type": "multipart/form-data"
        }
    });
}
export function getMailList(param) {
    return request({
        url: "/guapi/mail/list",
        method: "post",
        data: param,
    });
}
export function getMailDetail(param) {
    return request({
        url: "/guapi/mail/show",
        method: "post",
        data: param,
    });
}
export function sendMails(param) {
    return request({
        url: "/guapi/mail/send",
        method: "post",
        data: param,
    });
}
export function deleteMail(param) {
    return request({
        url: "/guapi/mail/delete",
        method: "post",
        data: param,
    });
}
export function sendMail(param) {
    return request({
        url: "/guapi/mail/sendOne",
        method: "post",
        data: param,
    });
}