import request from "@/utils/request";

export function getTaskList(param) {
	return request({
		url: "/guapi/task/list",
		method: "post",
		data: param
	});
}

export function addTask(param) {
	return request({
		url: "/guapi/task/save/" + false,
		method: "post",
		data: param
	});
}
export function editTask(param) {
	return request({
		url: "/guapi/task/save/" + true,
		method: "post",
		data: param
	});
}

export function deleteTask(param) {
	return request({
		url: "/guapi/task/delete",
		method: "post",
		data: param
	});
}

export function expiredTask(param) {
	return request({
		url: "/guapi/task/expired",
		method: "post",
		data: param
	});
}

export function cancelTask(param) {
	return request({
		url: "/guapi/task/cancel",
		method: "post",
		data: param
	});
}

export async function getMyTask(param, judgePublish) {
	return await request({
		url: "/guapi/task/myTask/" + judgePublish,
		method: "post",
		data: param,
		async: false
	});
}

export async function getTaskCount() {
	return await request({
		url: "/guapi/task/count",
		method: "post"
	});
}

export async function acceptTask(param) {
	return await request({
		url: "/guapi/task/accept",
		method: "post",
		data: param
	});
}
export async function submitTask(param) {
	return await request({
		url: "/guapi/task/finish",
		method: "post",
		data: param
	});
}
