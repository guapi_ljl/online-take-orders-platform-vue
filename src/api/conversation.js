import request from "@/utils/request";
export async function getMyConversation(data) {
	return await request({
		url: "/guapi/message/user",
		method: "post",
		data: data
	});
}
export async function sendMessage(data) {
	return await request({
		url: "/guapi/message/send",
		method: "post",
		data: data
	});
}
export async function setRead(data) {
	return await request({
		url: "/guapi/message/read",
		method: "post",
		data: data
	});
}
export async function getUnread(data) {
	return await request({
		url: "/guapi/message/unread",
		method: "post",
		data: data
	});
}
export async function conversationList(data) {
	return await request({
		url: "/guapi/message/list",
		method: "post",
		data: data
	});
}
