import request from "@/utils/request";

export function getMenuList(param) {
	return request({
		url: "/guapi/menu/list",
		method: "post",
		data: param
	});
}
export function addMenu(param) {
	return request({
		url: "/guapi/menu/save",
		method: "post",
		data: param
	});
}
export function deleteMenu(param) {
	return request({
		url: "/guapi/menu/delete",
		method: "post",
		data: param
	});
}
