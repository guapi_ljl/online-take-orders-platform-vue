import {
	createApp
} from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import vuex from 'vuex'
import router from '../src/route/index.js'
import axios from 'axios'
import 'element-plus/dist/index.css'
import jwt from 'jsonwebtoken'
import * as echarts from 'echarts'

import 'element-plus/theme-chalk/index.css'
import 'element-plus/theme-chalk/base.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import "lib-flexible/flexible"
const app = createApp(App)
app.axios = axios
app.use(ElementPlus)
app.use(router)
app.mount('#app')
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
	app.component(key, component)
}
