import Vue from "vue"
import {
	createRouter,
	createWebHistory
} from "vue-router"
import Route from "vue-router"
import Login from "@/views/common/login";
import Layout from "@/views/layout/index"
import UserList from "@/views/user/list"

const router = createRouter({
	history: createWebHistory(),
	routes: [{
			path: "/layout",
			name: "Layout",
			component: Layout,
			children: [{
				path: "/user/list",
				component: UserList
			}]
		},
		{
			path: "/",
			name: "Login",
			component: Login
		}
	]
})
export default router
