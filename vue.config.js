const {
    defineConfig
} = require('@vue/cli-service')
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const productionGzipExtensions = ['js', 'css'];
const webpack = require('webpack')
module.exports = defineConfig({
    configureWebpack: config => {
        config.plugins.push(new CompressionWebpackPlugin({
            algorithm: 'gzip',
            test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
            threshold: 10240,
            minRatio: 0.8
        }))
        config.devtool = false
    },

    chainWebpack: config => {
        config.plugins.delete("prefetch");
        // 移除 preload 插件
        config.plugins.delete('preload');
        // 压缩代码
        config.optimization.minimize(true);

        config.plugin("CompressionWebpackPlugin").use(
            new CompressionWebpackPlugin({
                algorithm: "gzip",
                test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
                threshold: 10240,
                minRatio: 0.8
            })
        );
    },
    transpileDependencies: false,
    lintOnSave: false,
    publicPath: "/",
    devServer: {
        // historyApiFallback: true,
        host: '0.0.0.0',
        allowedHosts: 'all',
        proxy: { //配置代理
            "/api": {
                //FIXME:需要改成后端ip
                target: "http://127.0.0.1:8099/guapi",
                // target: "http://8.134.160.39:8099/guapi",
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    "^/api": "",
                },
            }
        },
        port: 8081,
    },
    configureWebpack: {
        resolve: {
            alias: {},
            fallback: {
                //其他的如果不启用可以用 keyname :false，例如：crypto:false,
                "crypto": require.resolve("crypto-browserify"),
                "stream": require.resolve("stream-browserify")
            },
        },
        plugins: [new NodePolyfillPlugin()]
    },
    css: {
        extract: false,
        loaderOptions: {
            css: {},
            postcss: {
                postcssOptions: {
                    plugins: [
                        // 补全css前缀(解决兼容性)
                        require("autoprefixer")(),
                        // 把px单位换算成rem单位
                        require("postcss-pxtorem")({
                            rootValue: 50, // 换算的基数(设计图750的根字体为32)
                            selectorBlackList: [".van", ".my-van"],// 要忽略的选择器并保留为px。
                            propList: ["*"], //可以从px更改为rem的属性。
                            minPixelValue: 2 // 设置要替换的最小像素值。
                        })
                    ]
                }
            }
        }
    },
})
